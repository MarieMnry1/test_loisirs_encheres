# Test - Loisirs Encheres - Master 1 Dev WEB

## Information
```
à consulter en résolution : 1280x1024
```

## Installation du projet
```
npm install
```

### Compilation
```
npm run serve
```

### Notes

Test réalisé pour candidater au sein de l'entreprise

Encore beaucoup de choses à apprendre !

Mais des technos qui me plaisent beaucoup

La motivation la plus incroyable de Bordeaux ! =)

Une maturité suite à mon expérience professionnelle qui colle je pense parfaitement à votre start-up
